#!/usr/bin/python3.4
# -*-coding:utf-8 -*
import random
import math
import os

if os.name=='posix':
    os.system('clear')
elif os.name=='nt':
    os.system('cls')
else:
    pass

print('Bienvenue au Casino !')
try:
    sommedep=int(input('Veuillez entrer votre somme de départ en $ : '))
    assert sommedep > 10 and sommedep <= 10000
except ValueError:
    print('Veuillez entrer une somme valide')
    exit()
except AssertionError:
    print('Veuillez ne pas entrer moins de $10 et plus de $10000')


def paire(nb):
    if (nb%2)==0:
        return True;
    else:
        return False;

def scoring(mise,gain):
    print('Score: ',mise)
def prompt(inputval):
    if inputval=='Y'or inputval=='y' or inputval=='':
        return 1;
    else:
        return 0;
def main_game(sommedep):
    try:
        nb=int(input('Choisissez un numéro entre 0 et 49 : '))
        assert nb >= 0 and nb <= 49
    except ValueError:
        print('Veuillez entrer un numéro valide')
        return 0
    except AssertionError:
        print('Veuillez entrer un numéro compris entre 0 et 49')
        return 0
    try:
        mise=int(input('Choisissez une mise en $ : '))
        assert mise<=sommedep
    except ValueError:
        print('Veuillez entrer une mise numérique valide')
        return 0
    except AssertionError:
        print('Votre mise doit être inférieure ou égale à la somme qu\'il vous reste')
        return 0

    case=random.randrange(50)
    print('La Bille attérit dans la case ',case)


    if case==nb:
        print('Félicitations !\n Vous avez trouvé le nombre gagnant\nVous gagnez 3x votre mise initiale de $', mise, ' soit $',mise*3)
        gain=mise*3
        scoring(mise,gain)
        return gain
    elif paire(case)==True and paire(nb)==True:
        print('Vous n\'êtes pas tombés sur le numéro gagnant mais les deux numéros ont une couleur identique\nVous reprenez donc la moitié de la somme misée soit $',math.ceil(mise/2))
        gain=-(math.ceil(mise/2))
        scoring(mise,gain)
        return gain
    else:
        print('Pas de chance, les numéros et les couleurs ne correspondent pas...\nVous perdez la somme misée de $',mise)
        gain=-mise
        scoring(mise,gain)
        return gain

while sommedep>0:
    print('Vous avez $'+str(sommedep)+' sur vous, vous pouvez donc jouer!')
    if prompt(input('Voulez-vous Jouer ? (Y/n) '))==1:
        gain=main_game(sommedep)
        sommedep+=gain
        continue
    else:
        prompt(input('Sauvegarder votre score ? '))
        exit()
else:
    print('Vous avez tout Perdu ... Désolé Désolé.. :\'(')
